from django.db import models


# Create your models here.
class Teacher(models.Model):
    first_name = models.CharField(max_length=30, null=False)
    last_name = models.CharField(max_length=30, null=False)
    email = models.EmailField(max_length=60, null=True)
    birthdate = models.DateField(null=True)
    time_create = models.DateTimeField(auto_now_add=True)
