from django.db import models


# Create your models here.
class Group(models.Model):
    name = models.CharField(max_length=30, null=False)
    count = models.IntegerField(null=True)
    faculty = models.CharField(max_length=30, null=False)
    course = models.IntegerField(null=True, default=1)
